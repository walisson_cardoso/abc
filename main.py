import numpy as np
import matplotlib.pyplot as plt

from artificialBeeColony import ArtificialBeeColony as ABC

#%% Definitions

# Function to be optimized
def griewank(sol):
	D = len(sol)
	top  = 0.0
	prod = 1.0

	for i in range(D):
		xi = sol[i]
		top = top + (xi**2)/4000.0
		prod = prod * np.cos(xi/np.sqrt(i+1))
	
	return top - prod + 1

# Function name, search range and dimensionality
funcName = 'Griewank'
boundaries = [-600, 600]
dimensionality = 30

# Maximum number of function calls
MCF = 10000
# Instantiates colony
abc  = ABC(griewank, dimensionality, boundaries, MCF)


#%% Execution

# Executes the algorithm
convergence_curve = np.zeros(abc.maxCycle)
for i in range(abc.maxCycle):
	abc.iterate()
	convergence_curve[i] = abc.globalMin


# Execution info
print('function:', funcName)
print('Minimum found:', abc.globalMin)
print('Population size:', abc.np)
print('Number of cycles:', abc.maxCycle)

print('\nConvergence curve:')
plt.plot(convergence_curve, label='Minimum found')
plt.xlabel('Iteration')
plt.legend()
plt.show()
