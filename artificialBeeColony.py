'''

The Artificial Bee Colony Algorithm (ABC) was first described by Karaboga in:

 Karaboga, D. (2005). An idea based on honey bee swarm for numerical optimization
 (Tech. Rep. No. TR06). Kayseri/Turkey: Computer Engineering Department,
 Erciyes University.

Which describes a procedure to minimize a function based on the natural behaviour
of honey bees looking for profitable food sources.

This code implements the Artificial Bee Algorithm with adaptative parametrization.
The code is a modified version, based on the one that can be found in:
    https://abc.erciyes.edu.tr/software.htm

Please refer to the following dissertation for more information about this
project (in portuguese).

W. C. Gomes, Colônia Artificil de Abelhas com Parametrização Adaptativa,
Dissertação de Mestrado, Universidade Federal do Pará, Março de 2019.

'''

import numpy as np

class ArtificialBeeColony:
    def __init__(self, evalFunc, D, boundaries, MCF):
        # Initializes parameters based on passed MCF
        self.d, self.np, self.sn, self.maxCycle, self.limit = self.calcParams(D, MCF)
        self.evalFunc = evalFunc
        # Minimum found and its location
        self.globalMin = float("inf")
        self.globalPos = np.zeros(D)
        # Initiates the employed sources
        self.sources = [];
        for i in range(self.sn):
            self.sources.append(Source(evalFunc, D, self.d, boundaries))
            self.sources[i].init()

    def execute(self, maxCycle):
        # Executes all cycles at once
    	for i in range(maxCycle):
    		self.iterate()            
    
    def iterate(self):
        # An iteration consists on four steps
        self.sendEmployedBees()
        self.sendOnLookerBees()
        self.sendScoutBees()
        self.memorizeBestSource()
    
    
    def sendEmployedBees(self):
        # Causes perturbations on the sources' positions, trying to improve them.
        for i in range(self.sn):
            self.updateSolution(i)
    
    
    def sendOnLookerBees(self):
        # Onlooker bees are sent proportionally to the fitness of the
        # actual solutions. This function implements the steps:
        # 1_ calculate probability, 2_ send onlookers
        
        # Calculate probability of solutions are preferred by the onlookers
        maxFit = self.sources[0].fitness;
        for source in self.sources:
            if source.fitness > maxFit:
                maxFit = source.fitness
        
        prob = np.zeros(self.sn)
        for i in range(self.sn):
            fit = self.sources[i].fitness
            prob[i] = (0.9*(fit/maxFit))+0.1
        
        # Send onlooker bees to the solutions
        i = 0
        t = 0
        while t < self.sn:
            if np.random.rand() < prob[i]:
                t += 1
                self.updateSolution(i)
            i += 1
            i = i % self.sn
    
    
    def sendScoutBees(self):
        # Firstly, selects the source which had more unsuccessful attempts
        # to improve its position.
        # Then, this source is abandoned and the employed bee becomes a scout.
        # We will have at most one scout per iteration.
        maxTrialIndex = 0;
        for i in range(self.sn):
            if self.sources[i].trial > self.sources[maxTrialIndex].trial:
                maxTrialIndex = i
        
        if self.sources[maxTrialIndex].trial >= self.limit:
            self.sources[maxTrialIndex].init()
    
    
    def memorizeBestSource(self):
        # Memorizes best solution found
        for source in self.sources:
            if source.f < self.globalMin:
                self.globalMin = source.f
                self.globalPos = np.copy(source.x)
    
    
    def updateSolution(self, i):
        # Does the perturbation of i'th source position
        neighbor = np.random.randint(self.sn)
        while self.sn > 1 and neighbor == i:
            neighbor = np.random.randint(self.sn)
        self.sources[i].perturbation(self.sources[neighbor].x)
        
    
    def calcParams(self, D, MCF):
        #self.d, self.np, self.sn, self.maxCycle, self.limit
        d = int( min( 3.77 * 1e-5 * MCF + 1, D ) )
        NP = int( min( 1.08 * 1e-4 * MCF + 22.42, np.sqrt(MCF) ) )
        SN = int( NP / 2 )
        maxCycle = int( MCF / (NP+1) )
        limit = SN * D
        
        return ( d, NP, SN, maxCycle, limit)
    
    
    
class Source:
    def __init__(self, evalFunc, D, d, boundaries):
        # The constructor receives a functions for evaluation, number of
        # dimensions of the problem, the minimum and maximum limits of the
        # search space and number of dimensions to modify at once.
        self.evalFunc = evalFunc
        self.D = D
        self.d = d
        self.lb = boundaries[0]
        self.ub = boundaries[1]
        # Initializes the source's position, fitnesses' values and 
        # objective function value on the source.
        # Trial is initialized with zero.
        self.x = np.zeros(D)
        self.fitness = float("-inf")
        self.f = float("inf")
        self.trial = 0
    
    
    def init(self):
        # Initiates the source's position e calculates its fitness
        # (or reinitiates its position, in case it became a scout).
        self.x = np.random.uniform(self.lb, self.ub, self.D);
        self.f = self.evalFunc(self.x)
        self.fitness = self.calcFitness(self.f)
        self.trial = 0;

    
    def perturbation(self, refX):
        # Modifies the source's position using the reference source received (refX)
        # It selects randomly one coordinate of the space to be modified, verifies
        # the modification and updates the position only if the fitness is improved.
        newX = np.copy(self.x)
        
        params = np.arange(self.D)
        np.random.shuffle(params)
        
        for pc in params[:self.d]:
            shift = (np.random.rand()-0.5)*2 #[-1,+1]
            
            newX[pc] = newX[pc] + (newX[pc]-refX[pc])*shift
            newX[pc] = np.clip(newX[pc], self.lb, self.ub)
        
        newF = self.evalFunc(newX)
        newFitness = self.calcFitness(newF)
        # Updates the position if the fitness is improved.
        if newFitness > self.fitness:
            self.trial = 0
            self.x = np.copy(newX)
            self.f = newF
            self.fitness = newFitness
        else:
            self.trial += 1
            
    
    def calcFitness(self, f):
        # The fitness computation is directly based on the objective function.
        # There are no negative fitnesses. This is done in order to avoid
        # problems with the proportional selection.
        fitness = 0.0
        if f >= 0:
            fitness = 1.0/(f+1)
        else:
            fitness = 1.0 + abs(f)
        
        return fitness;
